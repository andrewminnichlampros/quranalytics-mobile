import { DrawerNavigationProp } from '@react-navigation/drawer';
import {
	Icon,
	Layout,
	Text,
	TopNavigation,
	TopNavigationAction,
} from '@ui-kitten/components';
import React from 'react';
import { RootStackParamList } from '../../Navigation';

type Props = {
	navigation: DrawerNavigationProp<RootStackParamList, 'StudentPortal'>;
};

export const StudentPortal = (props: Props) => {
	return (
		<>
			<TopNavigation
				title='Student Portal'
				accessoryLeft={() => (
					<TopNavigationAction
						icon={(iconProps) => <Icon {...iconProps} name='menu-outline' />}
						onPress={() => {
							props.navigation.toggleDrawer();
						}}
					/>
				)}
			/>
			<Layout
				style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
			>
				<Text>hello!</Text>
			</Layout>
		</>
	);
};
