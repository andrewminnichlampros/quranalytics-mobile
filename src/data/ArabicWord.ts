export type ArabicWordJsonData = [string, string, string];

export interface ArabicWordFields {
	arabicText: string;
	transliteration: string;
	translation: string;
}

export class ArabicWord {
	public arabicText: string;
	public transliteration: string;
	public translation: string;

	public constructor(fields: ArabicWordFields) {
		this.arabicText = fields.arabicText;
		this.transliteration = fields.transliteration;
		this.translation = fields.translation;
	}

	public static fromJson(data: ArabicWordJsonData): ArabicWord {
		return new ArabicWord({
			arabicText: data[0],
			translation: data[1],
			transliteration: data[2],
		});
	}
}
