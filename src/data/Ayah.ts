import { ArabicWord, ArabicWordJsonData } from './ArabicWord';

export type AyahJsonData = [string, string, string, ArabicWordJsonData[]];

export interface AyahFields {
	surahNumber: number;
	ayahNumber: number;
	arabicWords: ArabicWord[];
	translation: string;
}

export class Ayah {
	public surahNumber: number;
	public ayahNumber: number;
	public arabicWords: ArabicWord[];
	public translation: string;

	constructor(fields: AyahFields) {
		this.surahNumber = fields.surahNumber;
		this.ayahNumber = fields.ayahNumber;
		this.arabicWords = fields.arabicWords;
		this.translation = fields.translation;
	}

	static fromJson(data: AyahJsonData): Ayah {
		return new Ayah({
			surahNumber: parseInt(data[0]),
			ayahNumber: parseInt(data[1]),
			translation: data[2],
			arabicWords: data[3].map((wordData) => ArabicWord.fromJson(wordData)),
		});
	}
}
