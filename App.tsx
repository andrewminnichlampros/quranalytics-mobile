import React from 'react';
import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { NavigationContainer } from '@react-navigation/native';
import { DrawerContent, RootStackParamList } from './src/Navigation';
import { Reference } from './src/ui/screens/reference/Reference';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { StudentPortal } from './src/ui/screens/StudentPortal';

const Drawer = createDrawerNavigator<RootStackParamList>();

export default () => (
	<>
		<IconRegistry icons={EvaIconsPack} />
		<ApplicationProvider {...eva} theme={eva.light}>
			<NavigationContainer>
				<Drawer.Navigator
					initialRouteName='Reference'
					drawerContent={(props) => <DrawerContent {...props} />}
				>
					<Drawer.Screen name='Reference' component={Reference} />
					<Drawer.Screen name='StudentPortal' component={StudentPortal} />
				</Drawer.Navigator>
			</NavigationContainer>
		</ApplicationProvider>
	</>
);
